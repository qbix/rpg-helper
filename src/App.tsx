import React, {Component} from 'react';
import {SpellBook} from './components/spellbook';

export default class App extends Component<any, any> {
    render() {
        return (
            <div className="App">
                <SpellBook/>
            </div>
        );
    }
}
