import React, {ChangeEvent, Component} from 'react';
import './spell-book.scss';
import SpellPage from './SpellPage';
import {spells} from '../../repositories/SpellRepository';
import {Spell} from '../../model/spell/Spell';
import {Button, FormControlLabel, TextField} from '@material-ui/core';
import SpellList from './SpellList';

export default class SpellBook extends Component<any, State> {

    state = {
        pageSize: 6,
        page: 1,
        spells: spells.sort((spell1, spell2) => spell1.name.localeCompare(spell2.name)),
        spellsMenuOpen: true,
        selectedSpells: spells.map(spell => spell.id)
    };

    render() {
        const {pageSize, page, spells, spellsMenuOpen, selectedSpells} = this.state;
        return <div className="spell-book">
            <div className="config">
                <div className="buttons">
                    <Button color="primary" onClick={this.toggleSpellList}>Toggle List</Button>
                    <Button color="secondary" onClick={this.selectAllSpells}>Select All</Button>
                    <Button color="secondary" onClick={this.deselectAllSpells}>Select None</Button>
                </div>
                {spellsMenuOpen &&
                <SpellList spells={spells} selectedSpells={selectedSpells} toggleHandler={this.handleSpellToggle}/>}
                <div className="main">
                    <FormControlLabel control={<TextField name="pageSize"
                                                          value={pageSize}
                                                          onChange={this.handlePageSizeChange}/>}
                                      label="Page size:"
                                      labelPlacement="start"/>
                    <FormControlLabel control={<TextField name="page"
                                                          value={page}
                                                          onChange={this.handlePageChange}/>}
                                      label="Page:"
                                      labelPlacement="start"/>

                    <div className="total-spells">Total spells: {spells.length}</div>
                    <div className="selected-spells">Selected spells spells: {selectedSpells.length}</div>
                </div>

            </div>
            <SpellPage spells={this.getSpellPage()}/>
        </div>;
    }

    getSpellPage = (): Spell[] => {
        const {page, pageSize, spells, selectedSpells} = this.state;
        const last = page * pageSize > selectedSpells.length ? selectedSpells.length : page * pageSize;
        return spells
            .filter(spell => selectedSpells.includes(spell.id))
            .slice((page - 1) * pageSize, last);
    };

    handlePageSizeChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {target} = event;
        const pageSize = +target.value;
        if (!isNaN(pageSize)) {
            this.setAsyncState({
                ...this.state,
                pageSize: pageSize
            });
        }
    };

    handlePageChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {target} = event;
        const page = +target.value;
        if (!isNaN(page)) {
            this.setAsyncState({
                ...this.state,
                page: page
            });
        }
    };

    handleSpellToggle = (id: number) => {
        let selectedSpells = [...this.state.selectedSpells];
        console.log({id: id});
        if (selectedSpells.includes(id)) {
            const index = selectedSpells.indexOf(id, 0);
            selectedSpells.splice(index, 1);
        } else {
            selectedSpells.push(id);
        }
        this.setAsyncState({
            ...this.state,
            selectedSpells: selectedSpells
        });
    };

    toggleSpellList = () => {
        this.setAsyncState({
            ...this.state,
            spellsMenuOpen: !this.state.spellsMenuOpen
        });
    };

    deselectAllSpells = () => {
        this.setAsyncState({
            ...this.state,
            selectedSpells: []
        });
    };

    selectAllSpells = () => {
        this.setAsyncState({
            ...this.state,
            selectedSpells: this.state.spells.map(spell => spell.id)
        });
    };

    setAsyncState = (newState: State) =>
        new Promise((resolve) => this.setState(newState, resolve));
}

interface State {
    pageSize: number,
    page: number,
    selectedSpells: number[],
    spellsMenuOpen: boolean,
    spells: Spell[]
}
