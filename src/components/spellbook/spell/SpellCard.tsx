import React, {Component} from 'react';
import {Spell} from '../../../model/spell/Spell';
import parse from 'html-react-parser';

export default class SpellCard extends Component<Props, any> {
    render() {
        const spell = this.props.spell;
        return <div className="spell-card">
            <div className="name">{spell.name}</div>
            {(spell.level !== undefined && spell.school !== undefined) &&
            <div className="level">{this.spellLevel(spell.level)} - {spell.school}{spell.ritual && ' - ritual'}</div>}
            {(spell.castingTime && spell.range) &&
            <div className="row">
                <div className="casting-time"><label>Casting time:</label>{spell.castingTime}</div>
                <div className="range"><label>Range:</label>{spell.range}</div>
            </div>}
            {spell.components && <div className="components"><label>Components:</label>{spell.components}</div>}
            {spell.duration && <div className="duration"><label>Duration:</label>{spell.duration}</div>}
            <div className="description">{parse(spell.description)}</div>
            {spell.higherLevels &&
            <div className="higher-levels"><label>Higher levels:</label>{spell.higherLevels}</div>}
        </div>;
    }

    spellLevel = (level: number) => {
        const suffix = '-level';
        switch (level) {
            case 0:
                return 'Cantrip';
            case 1:
                return `${level}st${suffix}`;
            case 2:
                return `${level}nd${suffix}`;
            case 3:
                return `${level}rd${suffix}`;
            default:
                return `${level}th${suffix}`;
        }
    };

}

interface Props {
    spell: Spell
}
