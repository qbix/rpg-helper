import React, {Component, ReactNode} from 'react';
import {Spell} from '../../model/spell/Spell';
import {SpellCard} from './spell';

export default class SpellPage extends Component<Props, any> {
    render() {
        const spells = this.props.spells;
        return (
            <div className="spell-page">
                {this.spellCards(spells)}
            </div>
        );
    }

    spellCards = (spells: Spell[]): ReactNode[] => {
        return spells.map(spell => <SpellCard key={spell.name} spell={spell}/>);
    };

}

interface Props {
    spells: Spell[]
}
