import React, {ChangeEvent, Component, ReactNode} from 'react';
import {Spell} from '../../model/spell/Spell';
import {Checkbox, FormControlLabel, TextField} from '@material-ui/core';

export default class SpellList extends Component<Props, State> {
    state = {
        searchPhrase: ''
    };

    render() {
        const {spells, selectedSpells} = this.props;
        console.log(spells, selectedSpells);
        return <div className="spells">
            <TextField placeholder="Filter by name" value={this.state.searchPhrase} onChange={this.handleFilter}/>
            <div className="spell-list">
                {this.spellList(spells, selectedSpells)}
            </div>
        </div>;
    }

    spellList = (spells: Spell[], selectedSpells: number[]): ReactNode[] => {
        return spells
            .filter(spell => spell.name.toLowerCase().includes(this.state.searchPhrase.toLowerCase()))
            .sort((spell1, spell2) => spell1.name.localeCompare(spell2.name))
            .map((spell) => <FormControlLabel
                className="spell-list-item"
                key={spell.name}
                control={
                    <Checkbox name={spell.id.toString()}
                              onChange={this.handleCheckboxChange}
                              checked={selectedSpells.includes(spell.id)}
                              value={spell.name}/>
                }
                label={spell.name}/>);
    };

    handleCheckboxChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {target} = event;
        const spellId = +target.name;
        this.props.toggleHandler(spellId);
    };

    handleFilter = (event: ChangeEvent<HTMLInputElement>) => {
        this.setState({searchPhrase: event.target.value});
    };
}

interface Props {
    spells: Spell[]
    selectedSpells: number[]
    toggleHandler: (id: number) => void
}

interface State {
    searchPhrase: string
}
