import {Spell} from '../model/spell/Spell';
import {School} from '../model/spell/School';

export const spells: Spell[] = [
    {
        id: 0,
        name: 'Firebolt',
        ritual: false,
        level: 0,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'You hurl a mote of fire at a creature or object within range. Make a ranged spell attack against ' +
            'the target. On a hit, the target takes 1d10 fire damage. A flammable object hit by this spell ignites if ' +
            'it isn’t being worn or carried',
        higherLevels: 'This spell’s damage increases by 1d10 when you reach 5th level (2d10), 11th level (3d10),' +
            ' and 17th level (4d10).'
    },
    {
        id: 1,
        name: 'Green-Flame Blade',
        ritual: false,
        level: 0,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Self (5-foot radius)',
        components: 'S, M (a melee weapon worth at least 1 sp)',
        duration: 'Instantaneous',
        description: 'You brandish the weapon used in the spell’s casting and make a melee attack with it against one ' +
            'creature within 5 feet of you. On a hit, the target suffers the weapon attack’s normal effects, and you ' +
            'can cause green fire to leap from the target to a different creature of your choice that you can see ' +
            'within 5 feet of it. The second creature takes fire damage equal to your spellcasting ability modifier.',
        higherLevels: 'At 5th level, the melee attack deals an extra 1d8 fire damage to the target on a hit, and the ' +
            'fire damage to the second creature increases to 1d8 + your spellcasting ability modifier. Both damage ' +
            'rolls increase by 1d8 at 11th level (2d8 and 2d8) and 17th level (3d8 and 3d8)'
    },
    {
        id: 2,
        name: 'Lightning Lure',
        ritual: false,
        level: 0,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Self (15-foot radius)',
        components: 'V',
        duration: 'Instantaneous',
        description: 'You create a lash of lightning energy that strikes at one creature of your choice that you can ' +
            'see within 15 feet of you. The target must succeed on a Strength saving throw or be pulled up to 10 feet ' +
            'in a straight line toward you and then take 1d8 lightning damage if it is within 5 feet of you.',
        higherLevels: 'This spell\'s damage increases by 1d8 when you reach 5th level (2d8), 11th level (3d8), and' +
            ' 17th level (4d8).'
    },
    {
        id: 3,
        name: 'Prestidigitation',
        ritual: false,
        level: 0,
        school: School.TRANSMUTATION,
        castingTime: '1 action',
        range: '10 feet',
        components: 'V, s',
        duration: 'Up to 1 hour',
        description: 'This spell is a minor magical trick that novice spellcasters use for practice. You create one of ' +
            'the following magical effects within range: <br/>' +
            '+ You create an instantaneous, harmless sensory ' +
            'effect, such as a shower of sparks, a puff of wind, faint musical notes, or an odd odor.<br/>' +
            '+ You instantaneously clean or soil an object no larger than 1 cubic foot.<br/>' +
            '+ You chill, warm, or flavor up to 1 cubic foot of nonliving material for 1 hour.<br/>' +
            '+ You make a color, a small mark, or a symbol appear on an object or a surface for 1 hour.<br/>' +
            '+ You create a nonmagical trinket or an illusory image that can fit in your hand and that lasts until ' +
            'the end of your next turn.<br/>' +
            'If you cast this spell multiple times, you can have up to three of its non-instantaneous effects active ' +
            'at a time, and you can dismiss such an effect as an action.'
    },
    {
        id: 4,
        name: 'Alarm',
        ritual: true,
        level: 1,
        school: School.ABJURATION,
        castingTime: '1 action',
        range: '30 feet',
        components: 'V, S, M (a tiny bell and a piece of fine silver wire)',
        duration: '8 hours',
        description: 'You set an alarm against unwanted intrusion. Choose a door, a window, or an area within range' +
            ' that is no larger than a 20-foot cube. Until the spell ends, an alarm alerts you whenever a tiny or' +
            ' larger creature touches or enters the warded area. When you cast the spell, you can designate' +
            ' creatures that won’t set off the alarm. You also choose whether the alarm is mental or audible.<br/>' +
            'A mental alarm alerts you with a ping in your mind if you are within 1 mile of the warded area. This ping ' +
            'awakens you if you are sleeping. An audible alarm produces the sound of a hand bell for 10 seconds within ' +
            '60 feet.'
    },
    {
        id: 5,
        name: 'Burning Hands',
        ritual: false,
        level: 1,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Self (15-foot cone)',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'As you hold your hands with thumbs touching and fingers spread, a thin sheet of flames shoots ' +
            'forth from your outstretched fingertips. Each creature in a 15-foot cone must make a Dexterity saving ' +
            'throw. A creature takes 3d6 fire damage on a failed save, or half as much damage on a successful one.<br/>' +
            'The fire ignites any flammable objects in the area that aren’t being worn or carried.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the damage increases by 1d6' +
            ' for each slot level above 1st.'
    },
    {
        id: 6,
        name: 'Detect Magic',
        ritual: true,
        level: 1,
        school: School.DIVINATION,
        castingTime: '1 action',
        range: 'Self',
        components: 'V, S',
        duration: 'Concentration, up to 10 minutes',
        description: 'For the duration, you sense the presence of magic within 30 feet of you. If you sense magic in ' +
            'this way, you can use your action to see a faint aura around any visible creature or object in the area ' +
            'that bears magic, and you learn its school of magic, if any.<br/>' +
            'The spell can penetrate most barriers, but is blocked by 1 foot of stone, 1 inch of common metal, a thin ' +
            'sheet of lead, or 3 feet of wood or dirt.'
    },
    {
        id: 7,
        name: 'Find Familiar',
        ritual: true,
        level: 1,
        school: School.CONJURATION,
        castingTime: '1 hour',
        range: '10 feet',
        components: 'V, S, M (10 gp worth of charcoal, incense, and herbs that must be consumed by fire in a brass brazier)',
        duration: 'Concentration, up to 1 minute',
        description: 'You gain the service of a familiar, a spirit that takes an animal form you choose: bat, cat, ' +
            'crab, frog (toad), hawk, lizard, octopus, owl, poisonous snake, fish (quipper), rat, raven, sea horse, ' +
            'spider, or weasel. Appearing in an unoccupied space within range, the familiar has the statistics of the ' +
            'chosen form, though it is a celestial, fey, or fiend (your choice) instead of a beast.<br/>' +
            'Your familiar acts independently of you, but it always obeys your commands. In combat, it rolls its own ' +
            'initiative and acts on its own turn. A familiar can’t attack, but it can take other actions as' +
            ' normal.<br/>' +
            'When the familiar drops to 0 hit points, it disappears, leaving behind no physical form. It reappears ' +
            'after you cast this spell again.<br/>'
    },
    {
        id: 8,
        name: 'Find Familiar (pt2)',
        description: 'While your familiar is within 100 feet of you, you can communicate with it telepathically. ' +
            'Additionally, as an action, you can see through your familiar’s eyes and hear what it hears until the ' +
            'start of your next turn, gaining the benefits of any special senses that the familiar has. During this ' +
            'time, you are deaf and blind with regard to your own senses.<br/>' +
            'As an action, you can temporarily dismiss your familiar. It disappears into a pocket dimension ' +
            'where it awaits your summons. Alternatively, you can dismiss it forever. As an action while it is ' +
            'temporarily dismissed, you can cause it to reappear in any unoccupied space within 30 feet of you.<br/>' +
            'You can’t have more than one familiar at a time. If you cast this spell while you already have a ' +
            'familiar, you instead cause it to adopt a new form. Choose one of the forms from the above list. ' +
            'Your familiar transforms into the chosen creature.<br/>' +
            'Finally, when you cast a spell with a range of touch, your familiar can deliver the spell as if it had ' +
            'cast the spell. Your familiar must be within 100 feet of you, and it must use its reaction to deliver ' +
            'the spell when you cast it. If the spell requires an attack roll, you use your attack modifier for ' +
            'the roll.'
    },
    {
        id: 9,
        name: 'Identify',
        ritual: true,
        level: 1,
        school: School.DIVINATION,
        castingTime: '1 minute',
        range: 'Touch',
        components: 'V, S, M (a pearl worth at least 100 gp and an owl feather)',
        duration: 'Instantaneous',
        description: 'You choose one object that you must touch throughout the casting of the spell. If it is a ' +
            'magic item or some other magic-imbued object, you learn its properties and how to use them, whether it ' +
            'requires attunement to use, and how many charges it has, if any. You learn whether any spells are ' +
            'affecting the item and what they are. If the item was created by a spell, you learn which spell ' +
            'created it.<br/>' +
            'If you instead touch a creature throughout the casting, you learn what spells, if any, are currently ' +
            'affecting it.'
    },
    {
        id: 10,
        name: 'Mage Armor',
        ritual: false,
        level: 1,
        school: School.ABJURATION,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S, M (a piece of cured leather)',
        duration: '8 hours',
        description: 'You touch a willing creature who isn’t wearing armor, and a protective magical force ' +
            'surrounds it until the spell ends. The target’s base AC becomes 13 + its Dexterity modifier. ' +
            'The spell ends if the target dons armor or if you dismiss the spell as an action.'
    },
    {
        id: 11,
        name: 'Magic Missile',
        ritual: false,
        level: 1,
        school: School.ABJURATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'You create three glowing darts of magical force. Each dart hits a creature of your choice ' +
            'that you can see within range. A dart deals 1d4 + 1 force damage to its target. The darts all strike ' +
            'simultaneously and you can direct them to hit one creature or several.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the spell creates one ' +
            'more dart for each slot level above 1st.'
    },
    {
        id: 12,
        name: 'Shield',
        ritual: false,
        level: 1,
        school: School.ABJURATION,
        castingTime: '1 reaction, which you take when you are hit by an attack or targeted by the Magic Missile spell',
        range: '120 feet',
        components: 'V, S',
        duration: '1 round',
        description: 'An invisible barrier of magical force appears and protects you. Until the start of your ' +
            'next turn, you have a +5 bonus to AC, including against the triggering attack, and you take no ' +
            'damage from Magic Missile.'
    },
    {
        id: 13,
        name: 'Blur',
        ritual: false,
        level: 2,
        school: School.ILLUSION,
        castingTime: '1 action',
        range: 'Self',
        components: 'V',
        duration: 'Concentration, up to 1 minute',
        description: 'Your body becomes blurred, shifting and wavering to all who can see you. For the duration, ' +
            'any creature has disadvantage on attack rolls against you. An attacker is immune to this effect if ' +
            'it doesn\'t rely on sight, as with blindsight, or can see through illusions, as with truesight.'
    },
    {
        id: 14,
        name: 'Flaming Sphere',
        ritual: false,
        level: 2,
        school: School.CONJURATION,
        castingTime: '1 action',
        range: '60 feet',
        components: 'V, S, M (a bit of tallow, a pinch of brimstone, and a dusting of powdered iron)',
        duration: 'Concentration, up to 1 minute',
        description: 'A 5-foot-diameter sphere of fire appears in an unoccupied space of your choice within range ' +
            'and lasts for the duration. Any creature that ends its turn within 5 feet of the sphere must make a ' +
            'Dexterity saving throw. The creature takes 2d6 fire damage on a failed save, or half as much damage ' +
            'on a successful one.<br/>' +
            'As a bonus action, you can move the sphere up to 30 feet. If you ram the sphere into a creature, that ' +
            'creature must make the saving throw against the sphere’s damage, and the sphere stops moving this' +
            ' turn.<br/>' +
            'When you move the sphere, you can direct it over barriers up to 5 feet tall and jump it across pits up ' +
            'to 10 feet wide. The sphere ignites flammable objects not being worn or carried, and it sheds bright ' +
            'light in a 20-foot radius and dim light for an additional 20 feet.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, the damage increases by ' +
            '1d6 for each slot level above 2nd.'
    },
    {
        id: 15,
        name: 'Hold Person',
        ritual: false,
        level: 2,
        school: School.ENCHANTMENT,
        castingTime: '1 action',
        range: '60 feet',
        components: 'V, S, M (a small, straight piece of iron)',
        duration: 'Concentration, up to 1 minute',
        description: 'Choose a humanoid that you can see within range. The target must succeed on a Wisdom saving ' +
            'throw or be paralyzed for the duration. At the end of each of its turns, the target can make another ' +
            'Wisdom saving throw. On a success, the spell ends on the target.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, you can target one ' +
            'additional humanoid for each slot level above 2nd. The humanoids must be within 30 feet of each other ' +
            'when you target them.'
    },
    {
        id: 16,
        name: 'Invisibility',
        ritual: false,
        level: 2,
        school: School.ILLUSION,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S, M (an eyelash encased in gum arabic)',
        duration: 'Concentration, up to 1 hour',
        description: 'A creature you touch becomes invisible until the spell ends. Anything the target is wearing ' +
            'or carrying is invisible as long as it is on the target’s person. The spell ends for a target that ' +
            'attacks or casts a spell.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, you can target one ' +
            'additional creature for each slot level above 2nd.'
    },
    {
        id: 17,
        name: 'Misty step',
        ritual: false,
        level: 2,
        school: School.CONJURATION,
        castingTime: '1 bonus action',
        range: 'Self',
        components: 'V',
        duration: 'Instantaneous',
        description: 'Briefly surrounded by silvery mist, you teleport up to 30 feet to an unoccupied space that you can see.'
    },
    {
        id: 18,
        name: 'Fireball',
        ritual: false,
        level: 3,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '150 feet',
        components: 'V, S, M (a tiny ball of bat guano and sulfur)',
        duration: 'Instantaneous',
        description: 'A bright streak flashes from your pointing finger to a point you choose within range then ' +
            'blossoms with a low roar into an explosion of flame. Each creature in a 20-foot radius must make a ' +
            'Dexterity saving throw. A target takes 8d6 fire damage on a failed save, or half as much damage on a ' +
            'successful one. The fire spreads around corners. It ignites flammable objects in the area that aren’t ' +
            'being worn or carried',
        higherLevels: 'When you cast this spell using a spell slot of 4th level or higher, the damage increases by ' +
            '1d6 for each slot level above 3rd.'
    },
    {
        id: 19,
        name: 'Leomund\'s Tiny Hut',
        ritual: true,
        level: 3,
        school: School.EVOCATION,
        castingTime: '1 minute',
        range: 'Self (10-foot-radius hemisphere)',
        components: 'V, S, M (a small crystal bead)',
        duration: '8 hours',
        description: 'A 10-foot-radius immobile dome of force springs into existence around and above you and ' +
            'remains stationary for the duration. The spell ends if you leave its area.<br/>' +
            'Nine creatures of Medium size or smaller can fit inside the dome with you. The spell fails if its area includes a' +
            ' larger creature or more than nine creatures. Creatures and objects within the dome when you cast this' +
            ' spell can move through it freely. All other creatures and objects are barred from passing through it.' +
            ' Spells and other magical effects can’t extend through the dome or be cast through it. The atmosphere' +
            ' inside the space is comfortable and dry, regardless of the weather outside.<br/>' +
            ' Until the spell ends, you can command the interior to become dimly lit or dark. The dome is opaque' +
            ' from the outside, of any color you choose, but it is transparent from the inside.<br/>'
    },
    {
        id: 20,
        name: 'Sword Burst',
        ritual: false,
        level: 0,
        school: School.CONJURATION,
        castingTime: '1 action',
        range: 'Self (5-foot radius)',
        components: 'V',
        duration: 'Instantaneous',
        description: 'You create a momentary circle of spectral blades that sweep around you. All other creatures ' +
            'within 5 feet of you must succeed on a Dexterity saving throw or take 1d6 force damage.',
        higherLevels: 'This spell\'s damage increases by 1d6 when you reach 5th level (2d6), 11th level (3d6), ' +
            'and 17th level (4d6).'
    },
    {
        id: 21,
        name: 'Fog Cloud',
        ritual: false,
        level: 1,
        school: School.CONJURATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S',
        duration: 'Concentration, up to 1 hour',
        description: 'You create a 20-foot-radius sphere of fog centered on a point within range. The sphere spreads ' +
            'around corners, and its area is heavily obscured. It lasts for the duration or until a wind of moderate ' +
            'or greater speed (at least 10 miles per hour) disperses it.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the radius of the fog ' +
            'increases by 20 feet for each slot level above 1st.'
    },
    {
        id: 22,
        name: 'Thunderwave',
        ritual: false,
        level: 1,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Self (15-foot cube)',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'A wave of thunderous force sweeps out from you. Each creature in a 15-foot cube originating ' +
            'from you must make a Constitution saving throw. On a failed save, a creature takes 2d8 thunder damage ' +
            'and is pushed 10 feet away from you. On a successful save, the creature takes half as much damage and ' +
            'isn’t pushed.<br/>' +
            'In addition, unsecured objects that are completely within the area of effect are automatically pushed 10' +
            ' feet away from you by the spell’s effect, and the spell emits a thunderous boom audible out to 300 feet.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the damage increases ' +
            'by 1d8 for each slot level above 1st.'
    },
    {
        id: 23,
        name: 'Scorching Ray',
        ritual: false,
        level: 2,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'You create three rays of fire and hurl them at targets within range. You can hurl them at one ' +
            'target or several. Make a ranged spell attack for each ray. On a hit, the target takes 2d6 fire damage.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, you create one additional ' +
            'ray for each slot level above 2nd.'
    },
    {
        id: 24,
        name: 'Counterspell',
        ritual: false,
        level: 3,
        school: School.ABJURATION,
        castingTime: '1 reaction, which you take when you see a creature within 60 feet of you casting a spell',
        range: '60 feet',
        components: 'S',
        duration: 'Instantaneous',
        description: 'You attempt to interrupt a creature in the process of casting a spell. If the creature is ' +
            'casting a spell of 3rd level or lower, its spell fails and has no effect. If it is casting a spell of ' +
            '4th level or higher, make an ability check using your spellcasting ability. The DC equals 10 + the ' +
            'spell’s level. On a success, the creature’s spell fails and has no effect.',
        higherLevels: 'When you cast this spell using a spell slot of 4th level or higher, the interrupted spell has ' +
            'no effect if its level is less than or equal to the level of the spell slot you used.'
    },
    {
        id: 25,
        name: 'Dispel Magic',
        ritual: false,
        level: 3,
        school: School.ABJURATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'Choose any creature, object, or magical effect within range. Any spell of 3rd level or lower ' +
            'on the target ends. For each spell of 4th level or higher on the target, make an ability check using ' +
            'your spellcasting ability. The DC equals 10 + the spell’s level. On a successful check, the spell ends.',
        higherLevels: 'When you cast this spell using a spell slot of 4th level or higher, you automatically end ' +
            'the effects of a spell on the target if the spell’s level is equal to or less than the level of the ' +
            'spell slot you used.'
    },
    {
        id: 26,
        name: 'Eldritch Blast',
        ritual: false,
        level: 0,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'A beam of crackling energy streaks toward a creature within range. Make a ranged spell attack ' +
            'against the target. On a hit, the target takes 1d10 force damage.',
        higherLevels: 'The spell creates more than one beam when you reach higher levels: two beams at 5th level, ' +
            'three beams at 11th level, and four beams at 17th level. You can direct the beams at the same target or ' +
            'at different ones. Make a separate attack roll for each beam.'
    },
    {
        id: 27,
        name: 'Guidance',
        ritual: false,
        level: 0,
        school: School.DIVINATION,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S',
        duration: 'Concentration, up to 1 minute',
        description: 'You touch one willing creature. Once before the spell ends, the target can roll a d4 and add ' +
            'the number rolled to one ability check of its choice. It can roll the die before or after making the ' +
            'ability check. The spell then ends.'
    },
    {
        id: 28,
        name: 'Mending',
        ritual: false,
        level: 0,
        school: School.TRANSMUTATION,
        castingTime: '1 minute',
        range: 'Touch',
        components: 'V, S, M (two lodestones)',
        duration: 'Instantaneous',
        description: 'This spell repairs a single break or tear in an object you touch, such as a broken chain link, ' +
            'two halves of a broken key, a torn cloak, or a leaking wineskin. As long as the break or tear is no ' +
            'larger than 1 foot in any dimension, you mend it, leaving no trace of the former damage.<br/>' +
            'This spell can physically repair a magic item or construct, but the spell can’t restore magic to such ' +
            'an object.'
    },
    {
        id: 29,
        name: 'Minor Illusion',
        ritual: false,
        level: 0,
        school: School.ILLUSION,
        castingTime: '1 action',
        range: '30 feet',
        components: 'S, M (a bit of fleece)',
        duration: '1 minute',
        description: 'You create a sound or an image of an object within range that lasts for the duration. The ' +
            'illusion also ends if you dismiss it as an action or cast this spell again.<br/>' +
            'If you create a sound, its volume can range from a whisper to a scream. It can be your voice, someone' +
            ' else’s voice, a lion’s roar, a beating of drums, or any other sound you choose. The sound continues ' +
            'unabated throughout the duration, or you can make discrete sounds at different times before ' +
            'the spell ends.<br/>' +
            'If you create an image of an object—such as a chair, muddy footprints, or a small chest—it must be no' +
            ' larger than a 5-foot cube. The image can’t create sound, light, smell, or any other sensory effect. ' +
            'Physical interaction with the image reveals it to be an illusion, because things can pass through' +
            ' it.'
    },
    {
        id: 30,
        name: 'Minor Illusion (pt2)',
        description: 'If a creature uses its action to examine the sound or image, the creature can determine that it is an' +
            ' illusion with a successful Intelligence (Investigation) check against your spell save DC. If a ' +
            'creature discerns the illusion for what it is, the illusion becomes faint to the creature.'
    },
    {
        id: 31,
        name: 'Poison Spray',
        ritual: false,
        level: 0,
        school: School.CONJURATION,
        castingTime: '1 action',
        range: '10 feet',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'You extend your hand toward a creature you can see within range and project a puff of noxious ' +
            'gas from your palm. The creature must succeed on a Constitution saving throw or take 1d12 poison damage.',
        higherLevels: 'This spell’s damage increases by 1d12 when you reach 5th level (2d12), 11th level (3d12), ' +
            'and 17th level (4d12).'
    },
    {
        id: 32,
        name: 'Disguise Self',
        ritual: false,
        level: 1,
        school: School.ILLUSION,
        castingTime: '1 action',
        range: 'Self',
        components: 'V, S',
        duration: '1 hour',
        description: 'You make yourself – including your clothing, armor, weapons, and other belongings on your ' +
            'person – look different until the spell ends or until you use your action to dismiss it. You can seem ' +
            '1 foot shorter or taller and can appear thin, fat, or in between. You can’t change your body type, so ' +
            'you must adopt a form that has the same basic arrangement of limbs. Otherwise, the extent of the ' +
            'illusion is up to you.<br/>' +
            'The changes wrought by this spell fail to hold up to physical inspection. For example, if you use this ' +
            'spell to add a hat to your outfit, objects pass through the hat, and anyone who touches it would feel ' +
            'nothing or would feel your head and hair. If you use this spell to appear thinner than you are, the ' +
            'hand of someone who reaches out to touch you would bump into you while it was seemingly still in ' +
            'midair. To discern that you are disguised, a creature can use its action to inspect your appearance ' +
            'and must succeed on an Intelligence (Investigation) check against your spell save DC.'
    },
    {
        id: 33,
        name: 'Hex',
        ritual: false,
        level: 1,
        school: School.ENCHANTMENT,
        castingTime: '1 reaction',
        range: '90 feet',
        components: 'V, S, M (the petrified eye of a newt)',
        duration: 'Concentration, up to 1 hour',
        description: 'You place a curse on a creature that you can see within range. Until the spell ends, you deal ' +
            'an extra 1d6 necrotic damage to the target whenever you hit it with an attack. Also, choose one ability ' +
            'when you cast the spell. The target has disadvantage on ability checks made with the chosen' +
            ' ability.<br/>' +
            'If the target drops to 0 hit points before this spell ends, you can use a bonus action on a subsequent ' +
            'turn of yours to curse a new creature.<br/>' +
            'A Remove Curse cast on the target ends this spell early.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd or 4th level, you can maintain your ' +
            'concentration on the spell for up to 8 hours. When you use a spell slot of 5th level or higher, you can ' +
            'maintain your concentration on the spell for up to 24 hours.'
    },
    {
        id: 34,
        name: 'Mind Spike',
        ritual: false,
        level: 2,
        school: School.DIVINATION,
        castingTime: '1 action',
        range: '60 feet',
        components: 'S',
        duration: 'Concentration, up to 1 hour',
        description: 'You reach into the mind of one creature you can see within range. The target must make a ' +
            'Wisdom saving throw, taking 3d8 psychic damage on a failed save, or half as much damage on a ' +
            'successful one. On a failed save, you also always know the target\'s location until the spell ends, but' +
            ' only while the two of you are on the same plane of existence. While you have this knowledge, the ' +
            'target can’t become hidden from you, and if it’s invisible, it gains no benefit from that condition ' +
            'against you.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, the damage increases ' +
            'by 1d8 for each slot level above 2nd.'
    },
    {
        id: 35,
        name: 'Light',
        ritual: false,
        level: 0,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Touch',
        components: ' V, M (a firefly or phosphorescent moss)',
        duration: '1 hour',
        description: 'You touch one object that is no larger than 10 feet in any dimension. Until the spell ends, ' +
            'the object sheds bright light in a 20-foot radius and dim light for an additional 20 feet. The light ' +
            'can be colored as you like. Completely covering the object with something opaque blocks the light. The ' +
            'spell ends if you cast it again or dismiss it as an action.<br/>' +
            'If you target an object held or worn by a hostile creature, that creature must succeed on a Dexterity' +
            ' saving throw to avoid the spell.'
    },
    {
        id: 36,
        name: 'Vicious Mockery',
        ritual: false,
        level: 0,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '60 feet',
        components: ' V',
        duration: 'Instantaneous',
        description: 'You unleash a string of insults laced with subtle enchantments at a creature you can see ' +
            'within range. If the target can hear you (though it need not understand you), it must succeed on a ' +
            'Wisdom saving throw or take 1d4 psychic damage and have disadvantage on the next attack roll it makes ' +
            'before the end of its next turn.',
        higherLevels: 'This spell’s damage increases by 1d4 when you reach 5th level (2d4), 11th level (3d4), ' +
            'and 17th level (4d4).'
    },
    {
        id: 37,
        name: 'Charm Person',
        ritual: false,
        level: 1,
        school: School.ENCHANTMENT,
        castingTime: '1 action',
        range: '30 feet',
        components: 'V, S',
        duration: '1 hour',
        description: 'You attempt to charm a humanoid you can see within range. It must make a Wisdom saving ' +
            'throw, and does so with advantage if you or your companions are fighting it. If it fails the saving ' +
            'throw, it is charmed by you until the spell ends or until you or your companions do anything harmful ' +
            'to it. The charmed creature regards you as a friendly acquaintance. When the spell ends, the creature ' +
            'knows it was charmed by you.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, you can target one ' +
            'dditional creature for each slot level above 1st. The creatures must be within 30 feet of each other ' +
            'when you target them.'
    },
    {
        id: 38,
        name: 'Cure Wounds',
        ritual: false,
        level: 1,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'A creature you touch regains a number of hit points equal to 1d8 + your spellcasting ability ' +
            'modifier. This spell has no effect on undead or constructs.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the healing increases by ' +
            '1d8 for each slot level above 1st.'
    },
    {
        id: 39,
        name: 'Healing Word',
        ritual: false,
        level: 1,
        school: School.EVOCATION,
        castingTime: '1 bonus action',
        range: ' 60 feet',
        components: 'V',
        duration: 'Instantaneous',
        description: 'A creature of your choice that you can see within range regains hit points equal to ' +
            '1d4 + your spellcasting ability modifier. This spell has no effect on undead or constructs.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the healing increases ' +
            'by 1d4 for each slot level above 1st.'
    },
    {
        id: 40,
        name: 'Heroism',
        ritual: false,
        level: 1,
        school: School.ENCHANTMENT,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S',
        duration: 'Concentration, up to 1 minute',
        description: 'A willing creature you touch is imbued with bravery. Until the spell ends, the creature is ' +
            'immune to being frightened and gains temporary hit points equal to your spellcasting ability modifier ' +
            'at the start of each of its turns. When the spell ends, the target loses any remaining temporary hit' +
            ' points from this spell.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, you can target one ' +
            'additional creature for each slot level above 1st.'
    },
    {
        id: 41,
        name: 'Cloud of Daggers',
        ritual: false,
        level: 2,
        school: School.CONJURATION,
        castingTime: '1 action',
        range: '60 feet',
        components: 'V, S, M (a sliver of glass)',
        duration: 'Concentration, up to 1 minute',
        description: 'You fill the air with spinning daggers in a cube 5 feet on each side, centered on a point you ' +
            'choose within range. A creature takes 4d4 slashing damage when it enters the spell’s area for the ' +
            'first time on a turn or starts its turn there.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, the damage increases by ' +
            '2d4 for each slot level above 2nd.'
    },
    {
        id: 42,
        name: 'Enhance Ability',
        ritual: false,
        level: 2,
        school: School.TRANSMUTATION,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S, M (fur or a feather from a beast)',
        duration: 'Concentration, up to 1 minute',
        description: 'You touch a creature and bestow upon it a magical enhancement. Choose one of the following ' +
            'effects; the target gains the effect until the spell ends.<br/>' +
            '<b>Bear’s Endurance.</b> The target has advantage on Constitution checks. It also gains 2d6 temporary' +
            ' hit points, which are lost when the spell ends.<br/>' +
            '<b>Bull’s Strength.</b> The target has advantage on Strength checks, and their carrying capacity' +
            ' doubles.<br/>' +
            '<b>Cat’s Grace.</b> The target has advantage on Dexterity checks. It also doesn’t take damage from falling 20 ' +
            'feet or less if it isn’t incapacitated<br/>' +
            '<b>Eagle’s Splendor.</b> The target has advantage on Charisma checks.<br/>' +
            '<b>Fox’s Cunning.</b> The target has advantage on Intelligence checks.<br/>' +
            '<b>Owl’s Wisdom.</b> The target has advantage on Wisdom checks.',
        higherLevels: ' When you cast this spell using a spell slot of 3rd level or higher, you can target one ' +
            'additional creature for each slot level above 2nd.'
    },
    {
        id: 43,
        name: 'Mass Healing Word',
        ritual: false,
        level: 3,
        school: School.EVOCATION,
        castingTime: '1 bonus action',
        range: '60 feet',
        components: 'V',
        duration: 'Instantaneous',
        description: 'As you call out words of restoration, up to six creatures of your choice that you can see ' +
            'within range regain hit points equal to 1d4 + your spellcasting ability modifier. This spell has no ' +
            'effect on undead or constructs.',
        higherLevels: 'When you cast this spell using a spell slot of 4th level or higher, the healing increases by ' +
            '1d4 for each slot level above 3rd.'
    },
    {
        id: 44,
        name: 'Tasha\'s Hideous Laughter',
        ritual: false,
        level: 1,
        school: School.ENCHANTMENT,
        castingTime: '1 action',
        range: '30 feet',
        components: 'V, S, M (tiny tarts and a feather that is waved in the air)',
        duration: 'Concentration, up to 1 minute',
        description: 'A creature of your choice that you can see within range perceives everything as hilariously ' +
            'funny and falls into fits of laughter if this spell affects it. The target must succeed on a Wisdom ' +
            'saving throw or fall prone, becoming incapacitated and unable to stand up for the duration. A creature ' +
            'with an Intelligence score of 4 or less isn’t affected.<br/>' +
            'At the end of each of its turns, and each time it takes damage, the target can make another Wisdom' +
            ' saving throw. The target has advantage on the saving throw if it’s triggered by damage. On a success, ' +
            'the spell ends.'
    },
    {
        id: 45,
        name: 'Tasha\'s Caustic Brew',
        ritual: false,
        level: 1,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Self (30-foot line)',
        components: 'V, S, M (a bit of rotten food)',
        duration: 'Concentration, up to 1 minute',
        description: 'A stream of acid emanates from you in a line 30 feet long and 5 feet wide in a direction you ' +
            'choose. Each creature in the line must succeed on a Dexterity saving throw or be covered in acid for ' +
            'the spell’s duration or until a creature uses its action to scrape or wash the acid off itself or ' +
            'another creature. A creature covered in the acid takes 2d4 acid damage at start of each of its turns.',
        higherLevels: 'At Higher Levels. When you cast this spell using a spell slot 2nd level or higher, the damage' +
            ' increases by 2d4 for each slot level above 1st.'
    },
    {
        id: 46,
        name: 'Absorb Elements',
        ritual: false,
        level: 1,
        school: School.ABJURATION,
        castingTime: '1 reaction, which you take when you take acid, cold, fire, lightning, or thunder damage',
        range: 'Self (30-foot line)',
        components: 'S',
        duration: '1 round',
        description: 'The spell captures some of the incoming energy, lessening its effect on you and storing it for ' +
            'your next melee attack. You have resistance to the triggering damage type until the start of your next ' +
            'turn. Also, the first time you hit with a melee attack on your next turn, the target takes an extra 1d6 ' +
            'damage of the triggering type, and the spell ends.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the extra damage ' +
            'increases by 1d6 for each slot level above 1st.'
    },
    {
        id: 47,
        name: 'Heat Metal',
        ritual: false,
        level: 2,
        school: School.TRANSMUTATION,
        castingTime: '1 action',
        range: '60 feet',
        components: 'V, S, M (a piece of iron and a flame)',
        duration: 'Concentration, up to 1 minute',
        description: 'Choose a manufactured metal object, such as a metal weapon or a suit of heavy or medium metal ' +
            'armor, that you can see within range. You cause the object to glow red-hot. Any creature in physical ' +
            'contact with the object takes 2d8 fire damage when you cast the spell. Until the spell ends, you can use ' +
            'a bonus action on each of your subsequent turns to cause this damage again.<br/>' +
            'If a creature is holding or wearing the object and takes the damage from it, the creature must succeed' +
            ' on a Constitution saving throw or drop the object if it can. If it doesn’t drop the object, it has ' +
            'disadvantage on attack rolls and ability checks until the start of your next turn.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, the damage increases by ' +
            '1d8 for each slot level above 2nd.'
    },
    {
        id: 48,
        name: 'Alter Self',
        ritual: false,
        level: 2,
        school: School.TRANSMUTATION,
        castingTime: '1 action',
        range: 'Self',
        components: 'V, S',
        duration: 'Concentration, up to 1 hour',
        description: 'You assume a different form. When you cast the spell, choose one of the following options, the ' +
            'effects of which last for the duration of the spell. While the spell lasts, you can end one option as ' +
            'an action to gain the benefits of a different one.<br/>' +
            '<b>Aquatic Adaptation</b>. You adapt your body to an aquatic environment, sprouting gills, and growing' +
            ' webbing between your fingers. You can breathe underwater and gain a swimming speed equal to your ' +
            'walking speed.'
    },
    {
        id: 49,
        name: 'Alter Self (pt2)',
        description: '<b>Change Appearance.</b> You transform your appearance. You decide what you look like, including your' +
            ' height, weight, facial features, sound of your voice, hair length, coloration, and distinguishing ' +
            'characteristics, if any. You can make yourself appear as a member of another race, though none of your ' +
            'statistics change. You also don’t appear as a creature of a different size than you, and your basic ' +
            'shape stays the same; if you\'re bipedal, you can’t use this spell to become quadrupedal, for ' +
            'instance. At any time for the duration of the spell, you can use your action to change your ' +
            'appearance in this way again<br/>' +
            'Natural Weapons. You grow claws, fangs, spines, horns, or a different natural weapon of your choice. ' +
            'Your unarmed strikes deal 1d6 bludgeoning, piercing, or slashing damage, as appropriate to the ' +
            'natural weapon you chose, and you are proficient with your unarmed strikes. Finally, the natural ' +
            'weapon is magic and you have a +1 bonus to the attack and damage rolls you make using it.'
    },
    {
        id: 50,
        name: 'Tasha\'s Mind Whip',
        ritual: false,
        level: 2,
        school: School.ENCHANTMENT,
        castingTime: '1 action',
        range: '90 Feet',
        components: 'V',
        duration: '1 round',
        description: 'You psychically lash out at one creature you can see within range. The target must make ' +
            'an Intelligence saving throw. On a failed save, the target takes 3d6 psychic damage, and it can’t ' +
            'take a reaction until the end of its next turn. Moreover, on its next turn, it must choose whether ' +
            'it gets a move, an action, or a bonus action; it gets only one of the three. On a successful save, ' +
            'the target takes half as much damage and suffers none of the spell’s other effects.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, you can target one ' +
            'additional creature for each slot level above 2nd. The creatures must be within 30 feet of each other ' +
            'when you target them.'
    },
    {
        id: 51,
        name: 'Dragon\'s Breath',
        ritual: false,
        level: 2,
        school: School.TRANSMUTATION,
        castingTime: '1 bonus action',
        range: 'Touch',
        components: 'V, S, M (a hot pepper)',
        duration: 'Concentration, up to 1 minute',
        description: 'You touch one willing creature and imbue it with the power to spew magical energy from its ' +
            'mouth, provided it has one. Choose acid, cold, fire, lightning, or poison. Until the spell ends, ' +
            'the creature can use an action to exhale energy of the chosen type in a 15-foot cone. Each creature ' +
            'in that area must make a Dexterity saving throw, taking 3d6 damage of the chosen type on a failed ' +
            'save, or half as much damage on a successful one.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, the damage increases ' +
            'by 1d6 for each slot level above 2nd.'
    },
    {
        id: 52,
        name: 'Intellect Fortress',
        ritual: false,
        level: 3,
        school: School.ABJURATION,
        castingTime: '1 action',
        range: '30 feet',
        components: 'V',
        duration: 'Concentration, up to 1 hour',
        description: 'For the duration, you or one willing creature you can see within range has resistance to ' +
            'psychic damage, as well as advantage on Intelligence, Wisdom, and Charisma saving throws.',
        higherLevels: 'When you cast this spell using a spell slot of 4th level or higher, you can target one ' +
            'additional creature for each slot level above 3rd. The creatures must be within 30 feet of each other ' +
            'when you target them.'
    },
    {
        id: 53,
        name: 'Feign Death',
        ritual: true,
        level: 3,
        school: School.NECROMANCY,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S, M (a pinch of graveyard dirt)',
        duration: '1 hour',
        description: 'You touch a willing creature and put it into a cataleptic state that is indistinguishable from' +
            ' death.<br/>' +
            'For the spell’s duration, or until you use an action to touch the target and dismiss the spell, the' +
            ' target appears dead to all outward inspection and to spells used to determine the target’s status. ' +
            'The target is blinded and incapacitated, and its speed drops to 0. The target has resistance to all ' +
            'damage except psychic damage. If the target is diseased or poisoned when you cast the spell, or ' +
            'becomes diseased or poisoned while under the spell’s effect, the disease and poison have no effect ' +
            'until the spell ends.'
    },
    {
        id: 54,
        name: 'Arcane Eye',
        ritual: false,
        level: 4,
        school: School.DIVINATION,
        castingTime: '1 action',
        range: '30 feet',
        components: 'V, S, M (a bit of bat fur)',
        duration: 'Concentration, up to 1 hour',
        description: 'You create an invisible, magical eye within range that hovers in the air for the duration. ' +
            'You mentally receive visual information from the eye, which has normal vision and darkvision out to ' +
            '30 feet. The eye can look in every direction.<br/>' +
            'As an action, you can move the eye up to 30 feet in any direction. There is no limit to how far away' +
            ' from you the eye can move, but it can’t enter another plane of existence. A solid barrier blocks the ' +
            'eye’s movement, but the eye can pass through an opening as small as 1 inch in diameter.'
    },
    {
        id: 55,
        name: 'Shadow Of Moil',
        ritual: false,
        level: 4,
        school: School.NECROMANCY,
        castingTime: '1 action',
        range: 'Self',
        components: 'V, S, M (an undead eyeball encased in a gem worth at least 150 gp)',
        duration: 'Concentration, up to 1 minute',
        description: 'Flame-like shadows wreathe your body until the spell ends, causing you to become heavily ' +
            'obscured to others. The shadows turn dim light within 10 feet of you into darkness, and bright ' +
            'light in the same area to dim light.<br/>' +
            'Until the spell ends, you have resistance to radiant damage. In addition, whenever a creature within 10' +
            ' feet of you hits you with an attack, the shadows lash out at that creature, dealing it 2d8 necrotic damage.'
    },
    {
        id: 56,
        name: 'Witch Bolt',
        ritual: false,
        level: 1,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '30 feet',
        components: 'V, S, M (a twig from a tree that has been struck by lightning)',
        duration: 'Concentration, up to 1 minute',
        description: 'A beam of crackling, blue energy lances out toward a creature within range, forming a ' +
            'sustained arc of lightning between you and the target. Make a ranged spell attack against that ' +
            'creature. On a hit, the target takes 1d12 lightning damage, and on each of your turns for the ' +
            'duration, you can use your action to deal 1d12 lightning damage to the target automatically. The ' +
            'spell ends if you use your action to do anything else. The spell also ends if the target is ever ' +
            'outside the spell’s range or if it has total cover from you.',
        higherLevels: 'When you cast this spell using a spell slot of 2nd level or higher, the initial damage ' +
            'increases by 1d12 for each slot level above 1st.'
    },
    {
        id: 57,
        name: 'Melf\'s Acid Arrow',
        ritual: false,
        level: 2,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '90 feet',
        components: 'V, S, M (powdered rhubarb leaf and an adder’s stomach)',
        duration: 'Instantaneous',
        description: 'A shimmering green arrow streaks toward a target within range and bursts in a spray of acid. ' +
            'Make a ranged spell attack against the target. On a hit, the target takes 4d4 acid damage ' +
            'immediately and 2d4 acid damage at the end of its next turn. On a miss, the arrow splashes the target ' +
            'with acid for half as much of the initial damage and no damage at the end of its next turn.',
        higherLevels: 'When you cast this spell using a spell slot of 3rd level or higher, the damage (both ' +
            'initial and later) increases by 1d4 for each slot level above 2nd.'
    },
    {
        id: 58,
        name: 'See Invisibility',
        ritual: false,
        level: 2,
        school: School.DIVINATION,
        castingTime: '1 action',
        range: 'Self',
        components: 'V, S, M (a pinch of talc and a small sprinkling of powdered silver)',
        duration: '1 hour',
        description: 'For the duration, you see invisible creatures and objects as if they were visible, and you ' +
            'can see into the Ethereal Plane. Ethereal creatures and objects appear ghostly and translucent.'
    },
    {
        id: 59,
        name: 'Blink',
        ritual: false,
        level: 3,
        school: School.TRANSMUTATION,
        castingTime: '1 action',
        range: 'Self',
        components: 'V, S',
        duration: '1 minute',
        description: 'Roll a d20 at the end of each of your turns for the duration of the spell. On a roll of 11 ' +
            'or higher, you vanish from your current plane of existence and appear in the Ethereal Plane (the ' +
            'spell fails and the casting is wasted if you were already on that plane).<br/>' +
            'At the start of you next turn, and when the spell ends if you are on the Ethereal Plane, you return ' +
            'to an unoccupied space of your choice that you can see within 10 feet of the space you vanished from. ' +
            'If no unoccupied space is available within that range, you appear in the nearest unoccupied space ' +
            '(chosen at random if more that one space is equally near). You can dismiss this spell as an action.<br/>' +
            'While on the Ethereal Plane, you can see and hear the plane you originated from, which is cast in ' +
            'shades of gray, and you can’t see anything more than 60 feet away.You can only affect and be affected ' +
            'by other creatures on the Ethereal Plane. Creature that aren’t there can’t perceive you or interact ' +
            'with you, unless they have the ability to do so.'
    },
    {
        id: 60,
        name: 'Remove Curse',
        ritual: false,
        level: 3,
        school: School.ABJURATION,
        castingTime: '1 action',
        range: 'Touch',
        components: 'V, S',
        duration: 'Instantaneous',
        description: 'At your touch, all curses affecting one creature or object end. If the object is a cursed ' +
            'magic item, its curse remains, but the spell breaks its owner’s attunement to the object so it can ' +
            'be removed or discarded.'
    },
    {
        id: 61,
        name: 'Fear',
        ritual: false,
        level: 3,
        school: School.ILLUSION,
        castingTime: '1 action',
        range: 'Self (30-foot cone)',
        components: 'V, S, M (a white feather or the heart of a hen)',
        duration: 'Concentration, up to 1 minute',
        description: 'You project a phantasmal image of a creature’s worst fears. Each creature in a 30-foot cone ' +
            'must succeed on a Wisdom saving throw or drop whatever it is holding and become frightened for ' +
            'the duration.<br/>' +
            'While frightened by this spell, a creature must take the Dash action and move away from you by the ' +
            'safest available route on each of its turns, unless there is nowhere to move. If the creature ends ' +
            'its turn in a location where it doesn’t have line of sight to you, the creature can make a Wisdom ' +
            'saving throw. On a successful save, the spell ends for that creature.'
    },
    {
        id: 62,
        name: 'Fire Shield',
        ritual: false,
        level: 4,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: 'Self',
        components: 'V, S, M (a bit of phosphorus or a firefly)',
        duration: '10 minutes',
        description: 'Thin and wispy flames wreathe your body for the duration, shedding bright light in a 10-foot ' +
            'radius and dim light for an additional 10 feet. You can end the spell early by using an action to ' +
            'dismiss it.<br/>' +
            'The flames provide you with a warm shield or a chill shield, as you choose. The warm shield grants you' +
            ' resistance to cold damage, and the chill shield grants you resistance to fire damage.<br/>' +
            'In addition, whenever a creature within 5 feet of you hits you with a melee attack, the shield erupts' +
            ' with flame. The attacker takes 2d8 fire damage from a warm shield, or 2d8 cold damage from a cold shield.'
    },
    {
        id: 63,
        name: 'Wall of Fire',
        ritual: false,
        level: 4,
        school: School.EVOCATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S, M (a small piece of phosphorus)',
        duration: 'Concentration, up to 1 minute',
        description: 'You create a wall of fire on a solid surface within range. You can make the wall up to 60 ' +
            'feet long, 20 feet high, and 1 foot thick, or a ringed wall up to 20 feet in diameter, 20 feet high, ' +
            'and 1 foot thick. The wall is opaque and lasts for the duration.<br/>' +
            'When the wall appears, each creature within its area must make a Dexterity saving throw. On a failed ' +
            'save, a creature takes 5d8 fire damage, or half as much damage on a successful save.<br/>' +
            'One side of the wall, selected by you when you cast this spell, deals 5d8 fire damage to each ' +
            'creature that ends its turn within 10 feet of that side or inside the wall. A creature takes the same ' +
            'damage when it enters the wall for the first time on a turn or ends its turn there. The other side of ' +
            'the wall deals no damage.',
        higherLevels: 'When you cast this spell using a spell slot of 5th level or higher, the damage increases ' +
            'by 1d8 for each slot level above 4th.'
    },
    {
        id: 64,
        name: 'Polymorph',
        ritual: false,
        level: 4,
        school: School.TRANSMUTATION,
        castingTime: '1 action',
        range: '60 feet',
        components: 'V, S, M (a caterpillar cocoon)',
        duration: 'Concentration, up to 1 hour',
        description: 'This spell transforms a creature that you can see within range into a new form. An unwilling ' +
            'creature must make a Wisdom saving throw to avoid the effect. A shapechanger automatically succeeds on ' +
            'this saving throw.<br/>' +
            'The transformation lasts for the duration, or until the target drops to 0 hit points or dies. The ' +
            'new form can be any beast whose challenge rating is equal to or less than the target’s (or the ' +
            'target’s level, if it doesn’t have a challenge rating). The target’s game statistics, including mental ' +
            'ability scores, are replaced by the statistics of the chosen beast. It retains its alignment and personality.<br/>' +
            'The target assumes the hit points of its new form. When it reverts to its normal form, the creature ' +
            'returns to the number of hit points it had before it transformed. If it reverts as a result of dropping ' +
            'to 0 hit points, any excess damage carries over to its normal form. As long as the excess damage ' +
            'doesn’t reduce the creature’s normal form to 0 hit points, it isn’t knocked unconscious.<br/>'
    },
    {
        id: 65,
        name: 'Polymorph (pt2)',
        description: 'The creature is limited in the actions it can perform by the nature of its new form, and it can’t ' +
            'speak, cast spells, or take any other action that requires hands or speech.<br/>' +
            'The target’s gear melds into the new form. The creature can’t activate, use, wield, or otherwise ' +
            'benefit from any of its equipment. This spell can’t affect a target that has 0 hit points.'
    },
    {
        id: 66,
        name: 'Elemental Bane',
        ritual: false,
        level: 4,
        school: School.TRANSMUTATION,
        castingTime: '1 action',
        range: '90 feet',
        components: 'V, S',
        duration: 'Concentration, up to 1 minute',
        description: 'Choose one creature you can see within range, and choose one of the following damage types: ' +
            'acid, cold, fire, lightning, or thunder. The target must succeed on a Constitution saving throw or be ' +
            'affected by the spell for its duration. The first time each turn the affected target takes damage of ' +
            'the chosen type, the target takes an extra 2d6 damage of that type. Moreover, the target loses any ' +
            'resistance to that damage type until the spell ends.',
        higherLevels: 'When you cast this spell using a spell slot of 5th level or higher, you can target one ' +
            'additional creature for each slot level above 4th. The creatures must be within 30 feet of ' +
            'each other when you target them.'
    },
    {
        id: 67,
        name: 'Cloudkill',
        ritual: false,
        level: 5,
        school: School.CONJURATION,
        castingTime: '1 action',
        range: '120 feet',
        components: 'V, S',
        duration: 'Concentration, up to 10 minutes',
        description: 'You create a 20-foot-radius sphere of poisonous, yellow-green fog centered on a point you' +
            ' choose within range. The fog spreads around corners. It lasts for the duration or until strong wind' +
            ' disperses the fog, ending the spell. Its area is heavily obscured.<br/>' +
            'When a creature enters the spell’s area for the first time on a turn or starts its turn there, that' +
            ' creature must make a Constitution saving throw. The creature takes 5d8 poison damage on a failed save,' +
            ' or half as much damage on a successful one. Creatures are affected even if they hold their breath or' +
            ' don’t need to breathe.<br/>' +
            'The fog moves 10 feet away from you at the start of each of your turns, rolling along the surface of ' +
            'the ground. The vapors, being heavier than air, sink to the lowest level of the land, even pouring ' +
            'down openings.',
        higherLevels: 'When you cast this spell using a spell slot of 6th level or higher, the damage increases ' +
            'by 1d8 for each slot level above 5th.'
    },
    {
        id: 68,
        name: 'Teleportation Circle',
        ritual: false,
        level: 5,
        school: School.CONJURATION,
        castingTime: '1 minute',
        range: '10 feet',
        components: 'V, M (rare chalks and inks infused with precious gems with 50 gp, which the spell consumes)',
        duration: '1 round',
        description: 'As you cast the spell, you draw a 10-foot-diameter circle on the ground inscribed with sigils ' +
            'that link your location to a permanent teleportation circle of your choice whose sigil sequence you ' +
            'know and that is on the same plane of existence as you.<br/>' +
            'A shimmering portal opens within the circle you drew and remains open until the end of your next turn. ' +
            'Any creature that enters the portal instantly appears within 5 feet of the destination circle or in ' +
            'the nearest unoccupied space if that space is occupied.'
    },
    {
        id: 69,
        name: 'Teleportation Circle (pt2)',
        description: 'Many major temples, guilds, and other important places have permanent teleportation circles inscribed ' +
            'somewhere within their confines. Each such circle includes a unique sigil sequence – a string of ' +
            'magical runes arranged in a particular pattern. When you first gain the ability to cast this spell, ' +
            'you learn the sigil sequences for two destinations on the Material Plane, determined by the DM. You ' +
            'can learn additional sigil sequences during your adventures. You can commit a new sigil sequence ' +
            'to memory after studying it for 1 minute.<br/>' +
            'You can create a permanent teleportation circle by casting this spell in the same location every day ' +
            'for one year. You need not use the circle to teleport when you cast the spell in this way.'
    }
];
