import {School} from './School';

export interface Spell {
    id: number
    name: string
    level?: number
    ritual?: boolean
    school?: School
    castingTime?: string
    range?: string
    components?: string
    duration?: string
    description: string
    higherLevels?: string
}
